package com.zuitt;



public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        Contact info1 = new Contact();
        info1.setName("John Doe");
        info1.setContactNumber(new String[]{"+639152468596", "+639228547963"});
        info1.setAddress(new String[]{"Quezon City", "Makati City"});

        Contact info2 = new Contact();
        info2.setName("Jane Doe");
        info2.setContactNumber(new String[]{"+639162148573", "+639173698541"});
        info2.setAddress(new String[]{"Caloocan City", "Pasay City"});

        phonebook.getContacts().add(info1);
        phonebook.getContacts().add(info2);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("Phonebook is empty.");
        } else {
            for (Contact save : phonebook.getContacts()) {

                System.out.println(save.getName());

                System.out.println("----------------");
                System.out.println(save.getName() + " has the following registered numbers:");
                for (String number : save.getContactNumber()) {
                    System.out.println(number);
                }

                System.out.println("----------------");
                System.out.println(save.getName() + " has the following registered addresses:");
                System.out.println("My home address is in " + save.getAddress()[0]);
                System.out.println("My office address is in " + save.getAddress()[1]);

                System.out.println("=============================");
            }
        }
    }

}

